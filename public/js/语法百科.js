'use strict';

function playSound(file) {
    return new Audio(file).play()
}

const PLAYLIST_NAMES = [
    'domo',
    'libro',
    'domoj',
    'libroj',
    'hundo_amas_katon',
    'kato_amas_hundon',
    'alta',
    'bela',
    'ludi',
    'ridi',
];

window.onload = () => {
    let divElems = Array.from($('#grammar-highlight-box-1 > div > div'))
    for (let i = 0; i < divElems.length; i++){
        divElems[i].onclick = () => {
            playSound(`assets/${PLAYLIST_NAMES[i]}.mp3`).then()
        };
    }
};
